// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD-UGYa7cKCs-A3U1jf2E0-qlNsyZOv7AQ",
    authDomain: "pre-tamar.firebaseapp.com",
    projectId: "pre-tamar",
    storageBucket: "pre-tamar.appspot.com",
    messagingSenderId: "378019308599",
    appId: "1:378019308599:web:f61af0326bea6adbdb4bb2",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
