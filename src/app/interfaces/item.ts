export interface Item {
    id: string,
    first:string,
    second:number,
    third?:boolean,
    saved?:Boolean, //האם הערך נשמר בפיירבייס או לא
    result?:string //תוצאת החיזוי
}
