import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  
  deleteItem(userId:string, id:string){
    this.db.doc(`users/${userId}/elements/${id}`).delete();
  }

  itemCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  updateInevntory(id,isInInventory){
    console.log(id);
    console.log(isInInventory);
    this.authService.user.subscribe(
      user =>{
        let userId = user.uid;
        this.db.doc(`users/${userId}/elements/${id}`).update({third:isInInventory});
      }
    )   
  }

  updateRsult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/elements/${id}`).update(
      {
        result:result
      })
    }

  addItem(userId:string, first:string, second:number, third:boolean){ //מה שצריך כדי לעדכן את הדאטה בייס זה מי היוזר שמוסיף פריט, ומה הם השדות שלו ר 
    const item = {first:first, second:second, third:third}; //נגדיר משתנה קבוע של פריט
    this.userCollection.doc(userId).collection('elements').add(item); //פשוט עוד דרך במקום לכתוב במקום את הנתיב המלא כמו במחיקה ובעדכון,כמו שבנוי הדאטה בייס: פונים לקולקיישן יוזרס, ואז לדוקומנט יוזר איידי, ואז לקולקיישן אייטמס, ומוסיפים לשם את האייטם
  }

  public getItems(userId){ //הפונקציה בונה רפרנס לאייטמס קולקשין 
    this.itemCollection = this.db.collection(`users/${userId}/elements`);//לפונקציה קולקשין נכניס את הנתיב של האייטמס
    return this.itemCollection.snapshotChanges().pipe(map( //סנפשוטצ'אנגס בונה אובסרבבול שמעדכן אותנו על כל שינוי שיש ברשימת האייטמים
      collection => collection.map(
        document =>{
          const data = document.payload.doc.data();
          data.id =document.payload.doc.id; //אנחנו רוצים דם את האיי די של האייטם כדי שנוכל לבצע גם פעולות על האייטמיים
          return data; //מכיל גם איי די המשתנה דאטה מכיל את השדות והם נמצאים בפיילוד
        }
      )
    ))
    
    }  

  constructor(private db:AngularFirestore, private authService:AuthService) { }
}
