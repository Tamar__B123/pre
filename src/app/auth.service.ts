import { AngularFireModule } from '@angular/fire';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;

  getUser():Observable<User | null>{
    return this.user;
  }

  SignUp(email:string, password:string){
    return this.afAuth
        .createUserWithEmailAndPassword(email,password)
  }

  Logout(){
    this.afAuth.signOut().then(
      res => {
        this.router.navigate(['/bye'])
      }
    );  
  }

  login(email:string, password:string){
    return this.afAuth
        .signInWithEmailAndPassword(email,password)
  }

  constructor(public afAuth:AngularFireAuth,
    private router:Router) {
          this.user = this.afAuth.authState;
          console.log("auth service constructr worked");
    }
}
