import { PredicctionService } from './../predicction.service';
import { ItemsService } from './../items.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from '../auth.service';
import { Item } from '../interfaces/item';
import { Observable } from 'rxjs';
import { Input } from '@angular/core';


@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items=[];
  userId:string;
  items$;
  @Input() third: boolean = false; 
  first;
  second;
  id;
  saved;
  result; 
  showdelete:boolean = true;
  areYouSure:boolean = false;
  isInInventory:boolean = false;
  displayedColumns: string[] = ['name', 'price', 'Available?', 'Result', 'delete'];

  public onChange(event): void {  // event will give you full breif of action
    const newVal = event.target.value;
    console.log(newVal);
  }

  predict(){
    this.result = 'Load';
    this.predictionService.predict(this.second, this.third).subscribe( 
      res => {console.log(res);
        if(res = "False"){ 
          var result = 'Not available in stock';
        } else{ 
          var result = 'Available in stock';
        }
        this.result = result
        }//הצבת תוצאת ההחזרה
    );  
  }

  updateResult(){
    this.saved = true; 
    this.itemService.updateRsult(this.userId,this.id,this.result);
  }

  changeInventory(index){
    let isInInventory= this.items[index].isInInventory;
    this.itemService.updateInevntory(this.id, isInInventory);
  }
  
  deleteItem(index){
    let id = this.items[index].id;
    this.itemService.deleteItem(this.userId, id); 
  }


  show(){
    if(!this.areYouSure)
    this.showdelete = true;
  }

  hide(){
    this.showdelete = false;
  }  

  showAreYouSure(){
     this.areYouSure = true;
     this.showdelete = false;
  }

  hideAreYouSure(){
    this.areYouSure = false;
    this.showdelete = true;
  }  


  constructor(private db:AngularFireDatabase, public authService:AuthService, private itemService:ItemsService, private predictionService:PredicctionService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user =>{
        this.userId = user.uid;
        console.log(this.userId); 
        this.items$ = this.itemService.getItems(this.userId).subscribe(
          items =>{
            this.items = items;
            console.log(this.userId);
            console.log(this.items);
          })
      }
    )
  }
}
    