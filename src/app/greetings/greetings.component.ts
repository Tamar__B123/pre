import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.css']
})
export class GreetingsComponent implements OnInit {

  user: Observable<User | null>;

  getUserEmail():Observable<User | null>{
    return this.user;
  }

  constructor(public authService:AuthService) { }

  ngOnInit(): void {
  }

}
