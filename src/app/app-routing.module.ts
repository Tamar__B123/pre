import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ItemComponent } from './item/item.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ByeComponent } from './bye/bye.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ItemsComponent } from './items/items.component';
import { GreetingsComponent } from './greetings/greetings.component';

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'bye', component: ByeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: SignUpComponent},  
  { path: 'items', component: ItemsComponent},
  { path: '404', component: NotFoundComponent}, 
  { path: 'greetings', component: GreetingsComponent},
  {path: '**', redirectTo: '/404'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
