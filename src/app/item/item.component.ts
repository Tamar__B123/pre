import { PredicctionService } from './../predicction.service';
import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Item } from '../interfaces/item';
import { ItemsService } from '../items.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() data:any;

  @Input() third: boolean = false; 

  first;
  second;
  id;
  saved;
  result;
  userId:string; 
  showdelete:boolean = false;
  areYouSure:boolean = false;
  isInInventory:boolean = false;
  displayedColumns: string[] = ['first', 'second', 'third'];
  items:Item[];

  public onChange(event): void {  // event will give you full breif of action
    const newVal = event.target.value;
    console.log(newVal);
  }

  predict(){ //הפונקציה חוזה האם הלקוח יחזיר  או לא `
    this.result = 'Load';
    this.PredictionService.predict(this.second, this.third).subscribe( 
      res => {console.log(res);
        if(res = "False"){ 
          var result = 'Not available in stock';
        } else{ 
          var result = 'Available in stock';
        }
        this.result = result
        }//הצבת תוצאת ההחזרה
    );  
  }

  updateResult(){
    this.saved = true; 
    this.itemService.updateRsult(this.userId,this.id,this.result);
  }

  changeInventory(){
    console.log(this.isInInventory);
    this.itemService.updateInevntory(this.id, this.isInInventory);
  }
  
  deleteItem(){
    console.log(this.id);
    this.itemService.deleteItem(this.userId,this.id); 
  }


  show(){
    if(!this.areYouSure)
    this.showdelete = true;
  }

  hide(){
    this.showdelete = false;
  }  

  showAreYouSure(){
     this.areYouSure = true;
     this.showdelete = false;
  }

  hideAreYouSure(){
    this.areYouSure = false;
  }  

  constructor(public authService:AuthService, private itemService:ItemsService, private PredictionService:PredicctionService) { }

  ngOnInit(): void {
    this.id = this.data.id; 
    this.first = this.data.first;
    this.second = this.data.second;
    this.isInInventory = this.data.third; 
    this.saved = this.data.saved; 
    this.result = this.data.result; 
    this.authService.getUser().subscribe(
      user =>{
        this.userId = user.uid;
        console.log(this.userId); 
      }
    )
  }


}
