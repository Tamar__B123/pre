import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredicctionService {

  private url = 'https://yom2014ume.execute-api.us-east-1.amazonaws.com/beta';

  predict(second:number, third:boolean):Observable<any>{ //הפונקציה מקבלת שנות לימוד והכנסה ומבצעת את החיזוי
    let json = { //בניית הגייסון במבנה שהאיי פיי איי גייטואי מקבל
      "data": 
        {
          "second": second,
          "third": third        }
    }
    let body  = JSON.stringify(json); //הפיכה של הגייסון לסטרינג
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe( //מחזירים אובסרבבול
      map(res => { //נטען את מאפ 
        console.log("in the map"); //בדיקה שהגענו לפונקציה מאפ
        console.log(res); //נדפיס את התוצאה שמתקבלת מהשרת
        return res.body; //מחזירים את ההסתברות להחזרה     
      })
    );      
  }

  constructor(private http:HttpClient) { }
}
